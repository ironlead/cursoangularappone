**Primer Aplicación del Curso de Angular**  

Primer ejercicio del curso de Angular.  
Desarrollado bajo la versión de Angular 8.  
Cada commit contendrá una descripción de lo trabajado en dicho commit. Deberá haber una descripción detallada  
de cada commit en este archivo.  
---

## 1. Primer commit  

Se inicia proyecto con boostrap instalado. Se agregan sus referencia en el archivo angular.json  

Se instalaron boostrap, jquery y popper.  
1. sudo npm install bootstrap --save  
2. sudo npm install jquery --save  
3. sudo npm install popper.js --save  
Se agregaron 2 componentes: home y shared --> navbar.  
Comando para crear componentes:  
ng g c myComponent  
---